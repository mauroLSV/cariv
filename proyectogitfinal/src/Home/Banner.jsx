import BannerGit from '../assets/img/banner-git/BannerGit.png';

export function Banner() {
    return (
        <div >
            <img
                style={{ height: '740px', objectFit: 'cover' }}
                src={BannerGit}
                className='card-img '
                alt='banner'
            />
        </div>
    );
}